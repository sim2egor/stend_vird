/* */
/*	Основной */
#ifndef __LIFT_H
#define __LIFT_H

#include <mqx.h>
#include <bsp.h> 
#include "LED/led.h"	
#include "TSI/tsi.h"	

#define ENABLE_TEST_TASK				1
#define ENABLE_CONTROL_TASK			1
#define ENABLE_MONITOR_TASK			0
//--------------------------
// Опции отладки
/* Sensor Struct */
typedef struct
{
  int8_t mma7660_x;
  int8_t mma7660_y;
  int8_t mma7660_z;
  uint8_t mma7660_status;
  int8_t pot;
  int8_t temp_int;
  int8_t temp_dec;
} SENSOR_DATA;

/* Mode enum */
typedef volatile enum {
   TOUCH=0,
   TILT,
   GAME,
   MAX_MODES
} DEMO_MODE;

extern SENSOR_DATA Sensor;
extern DEMO_MODE mode;
extern LWSEM_STRUCT touch_sem;

extern int tch1,tch2,tch3,tch4;
// -----------------
enum 
{
	MAIN_TASK_HTTPD=1,
  SHELL_TASK,
	VIRD_TASK ,
	ADC_TASK
};
//Главная задача
void vird_task(uint32_t data); 
//Вспомогательная задача
void test_task(uint32_t data);
void adc_task(uint32_t data);
void wire_task(uint32_t data);

//Задачи мониторинга
void monitor_task(uint32_t data);

//Задачи HTTPDSRV
void main_task(uint32_t);
void shell_task(uint32_t);

// Семафор запуска эксперимента
extern LWSEM_STRUCT exp_sem;

// запись момента (напряжения)

extern void Set_UMoment(uint32_t data);
extern uint32_t Get_UMoment(void );
#endif
