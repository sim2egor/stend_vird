#ifndef __init_H
#define __init_H

#include <math.h>
#define ABS(x) x<0 ? -x : x
#define ROUND(x) x<0 ? x-0.5 : x+0.5

extern void int_service_routine(void *pin);

//----Управление двигателями Init.c
extern void Init_Ports (void);
extern void FTM_All (void);
extern void InitHWPIT (void);
extern void DAC_Init(void);
extern void Move_HW1(double L, uint32_t V, long Turn);
extern HWTIMER hwtimer1;   

extern LWGPIO_STRUCT 		GPIO_FirstDriveEnable, 	GPIO_FirstDriveSign, GPIO_FirstDriveZ,
												GPIO_Alrs,
												GPIO_FirstDriveStopR, GPIO_FirstDriveStopL,
												GPIO_FirstDriveRil, GPIO_FirstDriveFil,												
												GPIO_SecondDriveEnable, GPIO_SecondDriveSign;
extern LWGPIO_STRUCT puls1;
//interrupt
extern void PIT0_isr(void *);


void StartDriver(int Driver_Number);
void StopDriver(int Driver_Number);
void M3 (int Drive_Number);
void M4 (int Drive_Number);
void SetPDB (int Drive_Number, unsigned int RegPDB);
void SetSpeed(float speed);

void BrakeDriverCCW(int brake);
void BrakeDriverCW(int brake);
void Hold(int brake);
void AlarmS(int brake);

extern void Move (double L,double V);
extern void Move_alt (double L,double V);
extern double CurrentPos;
extern float V2;

volatile extern double T_All;
//___________________________________________________________________________
//-----------Общие переменные для работы 


extern LWSEM_STRUCT 	 lwsem, job_done;  ;						// семафор для перемещения (ПИТ0)

extern const double pi;
#endif

