#include "Stend_VIRD.h"		
#include "init.h"	
#include "LED/led.h"
#include "test_task.h"


void sin_speed(double x1, double x2, double speed);

double map(double angle, double in_min, double in_max, double out_min, double out_max);

void sin_speed(double x1, double x2, double speed){
		/* функция разгона двигателя  */
	int i,
			a ,							
			n,
			m;
	double vs, xs, 					 	//шаг ускорения, движения
				vt, xt , xtp = 0;		//скорость, позиция в настоящий момент цикла
	double k, L;
	double vmas[100],xtpmas[100]; //массив для скорости/перемещения
	double pif, addtmp;
	bool flag = 0;
	bool sign = 0;
	
	double as;
	
	
	a = 0;						//внутренний счетчик текущей точки внутри цикла	
	n = 100;						//кол-во точек на перемещение
	m = 20;							//кол-во точек на разгон(ускорение)
	vs =0.0; xs = 0.0; vt = 0.0; xt = 0,0; xtp = 0.0;
	
	pif = pi / m;	
	// Находим расстояние для перемещения
	L = x2 - x1;
	// Работа со знаком при премещении назад
	if (L < 0){
		sign = 0;
	} else {
		sign = 1;
	}
	L = ABS(L);
	x1 = ABS(x1);
	x2 = ABS(x2);
	// Находим шаг ускорения	
	vs = speed / n;
	// Выстяовляем первую точку (первый шаг)	
	vt = vs;
	// Находим шаг перемещения	
	xs = (x2-x1)/n;
	//Задаем начальное положение (x1)
	xt = x1;
	// Находим коэфициент	
	k = speed * 0.5;
	as = pi / m;
	// Основной цикл - разбиение на n точек		
	for (i = 0; i < 100; ++i) {
		xt = xt + xs;	
//		xtp = map(xt, 0, ((L * m) / n), 0, pi);
		xtp = xtp + as;
		if (vt < speed) {
			vt = k * sin(xtp - (pi/2)) + k;
			a++;
			
			if (vt < 10) 
				vt = 10;
			
			// Блок поддержания постоянной скорости
			if (vt > speed){ 
				vt = speed;
			}
		} 
		// Блок торможения		
		if (i >= (n - m)) {
			if (vt > 0) {
				if (flag == 0){
					addtmp = pi;
					flag = 1;
				}
			//	vt = k * sin(addtmp - (pi/2)) + k;
				
				if (vt < 5) 
					vt = 5;
				
				addtmp = addtmp - pif;
				xtp = addtmp;
			}
		}
		// Возвращаем знак итоговому результату	
		if (sign == 1) {	
			vmas[i] = vt;
			xtpmas[i] = xt;		
	//		Move(xt, vt);			
	//		xtpmas[i] = xtp;
		Move_alt(xt,vt);
		} else {
			vmas[i] = vt;
			xtpmas[i] = xt * (-1);	
		//	Move((xt * (-1)), vt);	
			Move_alt((xt * (-1)), vt);	
		}
//LED_toggle(4);	
	}
	flag = 0;
//	for (i = 0, i<100; ++i){
//		
//	}
}	
	
double map(double angle, double in_min, double in_max, double out_min, double out_max)
{
	return( (angle - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
}
