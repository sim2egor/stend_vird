#include "Stend_VIRD.h"
#include "Init.h"

void PIT0_isr(void *);

void PIT0_isr(void   *pit_isr_ptr)
	{ 
 		PIT_TCTRL0 = 2;
		_lwsem_post(&lwsem);
		PIT_TFLG0 = PIT_TFLG_TIF_MASK;          //clear flag
	}
	
int 	 PulseCount;	
float V2;	
volatile double T_All = 0;	
void Move (double L, double V) //V = 1000
{
	double Tl,V1;
	unsigned int RegPDB1;
	int iTl;

	
	V2 = 0;
	
	if (L<0)
		M4(1); // направление
	else	
		M3(1); // направление	
	
	L	= ABS(L);
	
//	Tl = (L/(float)V)*60; // sec находим время на выполнение перемещения
//	iTl = Tl*1000; // подготовливаем время для занесения в регистр или в задержку
	Tl =((L * 60)/(double)V);
	iTl = Tl*1000;
	V1 = ((L/Tl)*60);

/*Кол-во оборотов на требуемое расстояние*/

	V2 = ((V * Tl)/60);
	
	RegPDB1 = ROUND(((150*60)/ V1)-1);   // подготовливаем значение скорости для записи в регистр  управления двигателем

	PIT_LDVAL0  = (iTl/20)*1000000; 
	
	SetPDB(1, RegPDB1);
	StartDriver(1);

  PIT_TCTRL0 = 0x03;  //start PIT
	_bsp_int_init((IRQInterruptIndex)84, 4, 0, 1);
	
	_lwsem_wait(&lwsem);

	StopDriver(1);
	PulseCount = FTM1_CNT;
}
/*________________________________________________________________________________________________________________*/

double CurrentPos;
void Move_alt(double L, double V)
{
	double Tl,V1;
	unsigned int RegPDB1;
	int iTl;
	double Lt;
	
	Lt = L - CurrentPos;

	if (Lt<0)
		M4(1); // направление
	else	
		M3(1); // направление	
	
	Lt	= ABS(Lt);
	CurrentPos = L;
//	Lt = Lt - Llast;
//	Llast = L; 
//	Tl = (L/V)*60; // sec находим время на выполнение перемещения
	
//	Tl = (Lt/((double)V*5))*60;
			Tl =((Lt * 60)/(double)V);
	iTl = Tl*1000; // подготовливаем время для занесения в регистр или в задержку

	T_All = T_All + Tl;
	
//	V1 = ((Lt/Tl)*60/5);
			V1 = ((Lt/Tl)*60);
	
	RegPDB1 = ROUND(((150*60)/ V1)-1);   // подготовливаем значение скорости для записи в регистр  управления двигателем

	PIT_LDVAL0  = (iTl/20)*1000000; 
	
	SetPDB(1, RegPDB1);
	StartDriver(1);

  PIT_TCTRL0 = 0x03;  //start PIT
	_bsp_int_init((IRQInterruptIndex)84, 4, 0, 1);
	
	_lwsem_wait(&lwsem);

	StopDriver(1); 
	PulseCount = FTM1_CNT;
}
