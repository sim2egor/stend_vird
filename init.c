#include "Stend_VIRD.h"
#include "Init.h"

LWGPIO_STRUCT 		GPIO_FirstDriveEnable, 	GPIO_FirstDriveSign, GPIO_FirstDriveZ,
									GPIO_Alrs,
									GPIO_FirstDriveRil, GPIO_FirstDriveFil,
									GPIO_FirstDriveStopR, GPIO_FirstDriveStopL,
									GPIO_SecondDriveEnable, GPIO_SecondDriveSign;
LWGPIO_STRUCT puls1, son1, sign1;

const double pi = 3.14159265359;	
void Init_Ports (void)
{
	// Init SIM
	SIM_SCGC5 |= SIM_SCGC5_PORTA_MASK | SIM_SCGC5_PORTB_MASK | SIM_SCGC5_PORTC_MASK | SIM_SCGC5_PORTD_MASK | SIM_SCGC5_PORTE_MASK;
	SIM_SCGC2 |= SIM_SCGC2_DAC0_MASK | SIM_SCGC2_DAC1_MASK;
	SIM_SCGC6 |= SIM_SCGC6_PDB_MASK;
	SIM_SCGC6 |= SIM_SCGC6_FTM1_MASK;	
	SIM_SCGC3 |= SIM_SCGC3_FTM2_MASK;
	
		SIM_SCGC2 |= SIM_SCGC2_DAC0_MASK | SIM_SCGC2_DAC1_MASK;	//enable clock to DAC0
	DAC0_C0 = 0xC0;			//DACEN=1, DACRFS=1, DACTRGSEL=1  
	//Init Ports
	//A76 SON_1
	lwgpio_init(&GPIO_FirstDriveEnable, (GPIO_PORT_D | GPIO_PIN2), LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE);
	lwgpio_set_functionality(&GPIO_FirstDriveEnable, LWGPIO_MUX_D2_GPIO);
	//B79 SIGN_1
	lwgpio_init(&GPIO_FirstDriveSign, (GPIO_PORT_C| GPIO_PIN14), LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE);
	lwgpio_set_functionality(&GPIO_FirstDriveSign, LWGPIO_MUX_C14_GPIO);	
		
	//B71 ALRS
	lwgpio_init(&GPIO_Alrs, (GPIO_PORT_C | GPIO_PIN11), LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE);
	lwgpio_set_functionality(&GPIO_Alrs, LWGPIO_MUX_C11_GPIO);		
// Обратная связь для первого двигателя	
	
	//B44
	lwgpio_init(&GPIO_FirstDriveZ, (GPIO_PORT_D | GPIO_PIN14), LWGPIO_DIR_INPUT, LWGPIO_VALUE_NOCHANGE);
	lwgpio_set_functionality(&GPIO_FirstDriveZ, LWGPIO_MUX_D14_GPIO);
	lwgpio_set_attribute(&GPIO_FirstDriveZ, LWGPIO_ATTR_PULL_UP, LWGPIO_AVAL_ENABLE);
	lwgpio_int_init(&GPIO_FirstDriveZ, LWGPIO_INT_MODE_FALLING);
	
	_bsp_int_init(lwgpio_int_get_vector(&GPIO_FirstDriveZ), 4, 0, TRUE); 
  lwgpio_int_enable(&GPIO_FirstDriveZ, TRUE);	

	//B76	SON_2
	lwgpio_init(&GPIO_SecondDriveEnable, (GPIO_PORT_B | GPIO_PIN23), LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE);
	lwgpio_set_functionality(&GPIO_SecondDriveEnable, LWGPIO_MUX_B23_GPIO);	
	//A79	SIGN_2
	lwgpio_init(&GPIO_SecondDriveSign, (GPIO_PORT_D | GPIO_PIN5), LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE);
	lwgpio_set_functionality(&GPIO_SecondDriveSign, LWGPIO_MUX_D5_GPIO);


		//инит B67 (PTB 17) он же BSP_PULS1
	if (!lwgpio_init(&puls1, (GPIO_PORT_B | GPIO_PIN17), LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE))	{
				printf("Initializing Puls1 GPIO as output failed.\n");
				_task_block();
		}
	lwgpio_set_functionality(&puls1, LWGPIO_MUX_B17_GPIO);
	lwgpio_set_value(&puls1, LWGPIO_VALUE_HIGH); /* set pin to 1 */
		//инит A76 (PTD 02) он же BSP_SON1
	if (!lwgpio_init(&son1, (GPIO_PORT_D | GPIO_PIN2), LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE)){
			printf("Initializing SON1 GPIO as output failed.\n");
			_task_block();
	}
	lwgpio_set_functionality(&son1,   (LWGPIO_MUX_D2_GPIO));
	lwgpio_set_value(&son1, LWGPIO_VALUE_HIGH);

	if (!lwgpio_init(&sign1, (GPIO_PORT_C | GPIO_PIN14), LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE)){
			printf("Initializing SIGN1 GPIO as output failed.\n");
			_task_block();
	}
	lwgpio_set_functionality(&sign1, (LWGPIO_MUX_C14_GPIO));
	lwgpio_set_value(&sign1, LWGPIO_VALUE_HIGH); /* set pin to 1 */
}

void DAC_Init(void){
	SIM_SCGC2 |= SIM_SCGC2_DAC0_MASK | SIM_SCGC2_DAC1_MASK;	//enable clock to DAC0
	DAC0_C0 = 0xC0;			//DACEN=1, DACRFS=1, DACTRGSEL=1  
	DAC1_C0 = 0xC0;
	unsigned char temp;
	unsigned int result_16bit_2_12bit,data;
	
	data = 0;
	result_16bit_2_12bit = (uint32_t)(data>>3) ;
	//printf("\n Set default DAC0 is=%d", result_16bit_2_12bit);
	temp = result_16bit_2_12bit & 0xff;
	DAC0_DAT0L = DAC_DATL_DATA(temp);			//DAC0_DATL
	temp = result_16bit_2_12bit>>8 & 0xff;
	DAC0_DAT0H = DAC_DATH_DATA(temp);			//DAC0_DATH 
	DAC0_C0 |= DAC_C0_DACTRGSEL_MASK;			//software trigger the DAC0 to output DATA0[11:0]	
	
	data = 0;
	result_16bit_2_12bit = (uint32_t)(data>>3) ;
	//printf("\n Set default DAC1 is=%d", result_16bit_2_12bit);
	temp = result_16bit_2_12bit & 0xff;
	DAC1_DAT0L = DAC_DATL_DATA(temp);			//DAC0_DATL
	temp = result_16bit_2_12bit>>8 & 0xff;
	DAC1_DAT0H = DAC_DATH_DATA(temp);			//DAC0_DATH 
	DAC1_C0 |= DAC_C0_DACTRGSEL_MASK;			//software trigger the DAC0 to output DATA0[11:0]	
}

