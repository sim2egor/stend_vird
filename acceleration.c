#include "LIFT.h"		
#include "init.h"	
#include "LED/led.h"
#include "test_task.h"

void sin_speed(float x1, float x2, float speed);

float map(float angle, float in_min, float in_max, float out_min, float out_max);

void sin_speed(float x1, float x2, float speed){
	/*
	x1 - начальная позиция
	х2 - конечная позиция
	speed - скорость
	*/
	int i;
	int razgon_point = 30; 
	int 						n = 100; 	
	int						a = 0;				
	int temp_count;
	float L; 										
	float k;
	float vstep, xstep;        
	float vt, xt;								
	float xmas[100];            
	float vmas[100]; 						
	float addstep, addtemp;
	float addmas[30];
	L = x2 - x1;
	
	vstep = speed / n;

	xstep = L / n;

	k = speed * 0.5;
	

	vt = vstep;

	for (i = 0; i < n; ++i) {
		xt = xt + xstep;
		addtemp = map(xt, x1, ((L * razgon_point) / n), 0, pi);
		// Разделение на блоки работы цикла. Первый блок - разгон
		if (vt < speed) {
			vt = k * sin(addtemp - (pi/2)) + k;
			a++;
			// Второй блок - движение с постоянной скоростью
			if (vt > speed){ 
				vt = speed;
			}
		} 
		// Третий блок - торможение
		if (i >= (n - a)) {
			if (vt > 0) {
				vt = speed-1;
			}
		}
		vmas[i] = vt;
		xmas[i] = xt;
		addmas[i] = addtemp;	
	}
	_time_delay(100);
}

	
float map(float angle, float in_min, float in_max, float out_min, float out_max)
{
	return( (angle - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
}
