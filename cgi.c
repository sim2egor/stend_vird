/*HEADER**********************************************************************
*
* Copyright 2008 Freescale Semiconductor, Inc.
* Copyright 1989-2008 ARC International
*
* This software is owned or controlled by Freescale Semiconductor.
* Use of this software is governed by the Freescale MQX RTOS License
* distributed with this Material.
* See the MQX_RTOS_LICENSE file distributed for more details.
*
* Brief License Summary:
* This software is provided in source form for you to use free of charge,
* but it is not open source software. You are allowed to use this software
* but you cannot redistribute it or derivative works of it in source form.
* The software may be used only in connection with a product containing
* a Freescale microprocessor, microcontroller, or digital signal processor.
* See license agreement file for full license terms including other
* restrictions.
*****************************************************************************
*
* Comments:
*
*   Example of shell using RTCS.
*
*
*END************************************************************************/

#include "Stend_VIRD.h"
#include "httpsrv.h"
//#include <stdio.h>
static int spd=100;
static _mqx_int cgi_ipstat(HTTPSRV_CGI_REQ_STRUCT* param);
static _mqx_int cgi_rtc_data(HTTPSRV_CGI_REQ_STRUCT* param);
static _mqx_int form(HTTPSRV_CGI_REQ_STRUCT* param);
void set_speed(int spd);

const HTTPSRV_CGI_LINK_STRUCT cgi_lnk_tbl[] = {
    { "ipstat",         cgi_ipstat,   0},
    { "rtcdata",        cgi_rtc_data, 1500},
		{ "form",        form, 1500},
    { 0, 0 }    // DO NOT REMOVE - last item - end of table
};

////-----------------------------------------------------------------------------
////-----------------------------------------------------------------------------
////-----------------------------------------------------------------------------
static int get_varval(char *var_str, char *var_name, char *var_val, _mqx_int var_val_len)
{
    char *var = var_str;
    int res = 0;
    int idx;

    var_val[0] = 0;

    while ((var = strstr(var, var_name)) != 0)
    {
        if (*(var + strlen(var_name)) == '=')
        {
            var += strlen(var_name) + 1;    // +1 because '='

            idx = (int)strcspn(var, "&");
            strncpy(var_val, var, (unsigned long)idx);
            var_val[idx] = 0;
            res = 1;
            break;
        }
        else
        {
            var = strchr(var, '&');
        }
    }

    return res;
}

static _mqx_int form(HTTPSRV_CGI_REQ_STRUCT* param)
{
	uint32_t  len = 0;
    char     speed[40];
    uint32_t  spd = 0;
		_mqx_uint result;
    bool  bParams = FALSE; 
    char     buffer[100];
    HTTPSRV_CGI_RES_STRUCT response;
    
    if (param->request_method != HTTPSRV_REQ_POST)
    {
        return(0);
    }
    
    len = param->content_length;
    len = HTTPSRV_cgi_read(param->ses_handle, buffer, (len > sizeof(buffer)) ? sizeof(buffer) : len);
    
    if (param->content_length)
    {
        buffer[len] = 0;
        param->content_length -= len;
        len = 0;
        
        if (get_varval(buffer, "SPEED", speed, sizeof(speed)) ) {
            
            bParams =  TRUE;
							// считать скорость
						if (sscanf(speed, "%d", &spd) >= 1)
            {
							printf("Speed 0%d \n",spd);
              set_speed(spd);
            }
         
            
        }
				result = _lwsem_post(&exp_sem);
				if (result != MQX_OK){
					printf("Not create semaphore\n");
					_mqx_exit(0);
				}
				printf("Post semafor!~\n");
    }
    response.ses_handle = param->ses_handle;
    response.content_type = HTTPSRV_CONTENT_TYPE_HTML;
    response.status_code = 200;
    response.data = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">"
        "<html><head><title>Settings response</title>"
        "<meta http-equiv=\"REFRESH\" content=\"0;url=index.html\"></head>\n<body>\n";
    response.data_length = strlen(response.data);
    response.content_length = 0;
    HTTPSRV_cgi_write(&response);
    
    if (!bParams)
    {
        response.data = "No parameters received.<br>\n";
        response.data_length = strlen(response.data);
        HTTPSRV_cgi_write(&response);
    }
    response.data = "<br><br>\n</body></html>";
    response.data_length = strlen(response.data);
    HTTPSRV_cgi_write(&response);    
    return (response.content_length);
}
////-----------------------------------------------------------------------------
////-----------------------------------------------------------------------------

static _mqx_int cgi_rtc_data(HTTPSRV_CGI_REQ_STRUCT* param)
{
    #define BUFF_SIZE sizeof("00\n00\n00\n")
    HTTPSRV_CGI_RES_STRUCT response;
    TIME_STRUCT time;
    uint32_t min;
    uint32_t hour;
    uint32_t sec;
    
    char str[BUFF_SIZE];
    uint32_t length = 0;
    
    if (param->request_method != HTTPSRV_REQ_GET)
    {
        return(0);
    }

    _time_get(&time);
    
    sec = time.SECONDS % 60;
    min = time.SECONDS / 60;
    hour = min / 60;
    min %= 60;

    response.ses_handle = param->ses_handle;
    response.content_type = HTTPSRV_CONTENT_TYPE_PLAIN;
    response.status_code = 200;
    /* 
    ** When the keep-alive is used we have to calculate a correct content length
    ** so the receiver knows when to ACK the data and continue with a next request.
    ** Please see RFC2616 section 4.4 for further details.
    */
    
    /* Calculate content length while saving it to buffer */                                  
    length = snprintf(str, BUFF_SIZE, "%ld\n%ld\n%ld\n", hour, min, sec);          
    response.data = str;
    response.data_length = length;
    response.content_length = response.data_length;
    /* Send response */
    HTTPSRV_cgi_write(&response);
    return (response.content_length);
}

_mqx_int cgi_ipstat(HTTPSRV_CGI_REQ_STRUCT* param)
{
    HTTPSRV_CGI_RES_STRUCT response;

    if (param->request_method != HTTPSRV_REQ_GET)
    {
        return(0);
    }

    response.ses_handle = param->ses_handle;
    response.content_type = HTTPSRV_CONTENT_TYPE_PLAIN;
    response.status_code = 200;

#if RTCSCFG_ENABLE_IP_STATS && RTCSCFG_ENABLE_IP4
    {
        uint32_t length = 0;
        /* We need to store 10 literals + '\n' (in worst case) for 19 unsigned integers */
        char str[sizeof(char)*19*(10+1)+1];

        /* Read IP statistics */
        IP_STATS_PTR ip = IP_stats();

        /* Print data to buffer */
        length = snprintf(str, sizeof(str),
            "%ld\n%ld\n%ld\n%ld\n%ld\n%ld\n%ld\n%ld\n%ld\n%ld\n%ld\n%ld\n%ld\n%ld\n%ld\n%ld\n%ld\n%ld\n%ld\n",
            spd,
            ip->ST_RX_DELIVERED,
            ip->ST_RX_FORWARDED,
            ip->COMMON.ST_RX_MISSED, 
            ip->COMMON.ST_RX_ERRORS,
            ip->COMMON.ST_RX_DISCARDED,
            ip->ST_RX_HDR_ERRORS,
            ip->ST_RX_ADDR_ERRORS,
            ip->ST_RX_NO_PROTO,
            ip->ST_RX_FRAG_RECVD,
            ip->ST_RX_FRAG_REASMD,
            ip->ST_RX_FRAG_DISCARDED,
            ip->COMMON.ST_TX_TOTAL,
            ip->COMMON.ST_TX_MISSED,
            ip->COMMON.ST_TX_ERRORS,
            ip->COMMON.ST_TX_DISCARDED,
            ip->ST_TX_FRAG_FRAGD,
            ip->ST_TX_FRAG_SENT,
            ip->ST_TX_FRAG_DISCARDED);

        /* Write header and buffer with data */
        
        response.data = str;
        response.data_length = length;
        response.content_length = response.data_length;
        HTTPSRV_cgi_write(&response);
    }
#else
    {
        int i;

        response.content_length = strlen("unk\n")*19;
        response.data = "unk\n";
        response.data_length = 4;
        for (i = 0; i < 19; i++)
        {
            HTTPSRV_cgi_write(&response);
        }
    }
#endif
    return (response.content_length);
}


void set_speed(int velocity)
{
	spd = velocity;
}
