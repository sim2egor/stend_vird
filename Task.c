/* */
/* Задачи */
#include "Stend_VIRD.h"

const TASK_TEMPLATE_STRUCT MQX_template_list[] =
{
   /* Task Index,  		 Function,         Stack,  Priority, Name,       Attributes,             Param,  Time Slice */
    { MAIN_TASK_HTTPD, main_task,          2000,    8,       "Main",     MQX_AUTO_START_TASK,    0,      0           },
    { SHELL_TASK,      shell_task,           2000,    9,       "Shell_task", 0,                    0,      0           },
    { VIRD_TASK,   		 vird_task,        	1400,    10,      "VIRD",     0                    ,    0,      0           },
    { ADC_TASK,    		 adc_task,  					2000,  	 8, 	    "adc",			MQX_FLOATING_POINT_TASK,0,  	  0					  },		
		{0}
};
