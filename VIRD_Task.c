#include "Stend_VIRD.h"

#include "init.h"


LWSEM_STRUCT lwsem;
_task_id ADCTASKID;
_task_id WIRETASKID;


/* TSI block*/
volatile unsigned char keypressed;

SENSOR_DATA Sensor;
DEMO_MODE mode;
LWSEM_STRUCT exp_sem;
LWSEM_STRUCT touch_sem;
/*-----------------------------*/

bool FLAG;
/*-----------------------------*/
int c_tmp, pulse, first_rev;

void vird_task(uint32_t data)
{
	_mqx_uint  result;
	// Проверить(перенести) семафор 
	 result = _lwsem_create(&lwsem, 0); 
   if (result != MQX_OK) {
      printf("\nCreating sem failed: 0x%X", result);
      _task_block();
   }
	 result = _lwsem_create(&exp_sem, 0); 
	 if (result != MQX_OK) {
      printf("\nCreating sem failed: 0x%X", result);
      _task_block();
   }
	 
	Init_Ports();
	LED_init();
	DAC_Init();
  TSI_Start();	 
	 
// Создание задач в режиме блокировки 
	ADCTASKID = _task_create(0,ADC_TASK,0);	 
	 

	while(1){
		result=_lwsem_wait(&exp_sem);
		if (result != MQX_OK) {
			printf("\n_lwsem_wait failed: 0x%X", result);
			_mqx_exit(0);
		}
		// пришла команда начать эксперимент	
		printf("Start experiment!!! \n"); 
	}	
    _task_block();
}		

