#include <mqx.h>
#include <bsp.h> 
#include "led.h"

LWGPIO_STRUCT led1,led2,led3,led4, btn1, btn2;
LWGPIO_STRUCT GPIO_FirstDriveBeginLimit, GPIO_SecondDriveBeginLimit, 
							GPIO_FirstDriveEndLimit,   GPIO_SecondDriveEndLimit;

void LED_init(void);

void LED_init(void)
{
	//________________________Button and LED________________________________

		lwgpio_init(&led1, BSP_LED1, LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE);
		lwgpio_set_functionality(&led1, BSP_LED1_MUX_GPIO);
		lwgpio_set_value(&led1, LWGPIO_VALUE_HIGH);
		
		lwgpio_init(&led2, BSP_LED2, LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE);
		lwgpio_set_functionality(&led2, BSP_LED2_MUX_GPIO);
		lwgpio_set_value(&led2, LWGPIO_VALUE_HIGH);
		
		lwgpio_init(&led3, BSP_LED3, LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE);
		lwgpio_set_functionality(&led3, BSP_LED3_MUX_GPIO);
		lwgpio_set_value(&led3, LWGPIO_VALUE_HIGH);

		lwgpio_init(&led4, BSP_LED4, LWGPIO_DIR_OUTPUT, LWGPIO_VALUE_NOCHANGE);
		lwgpio_set_functionality(&led4, BSP_LED4_MUX_GPIO);
		lwgpio_set_value(&led4, LWGPIO_VALUE_HIGH);
		/* opening pins for input */
	  if (!lwgpio_init(&btn1, BSP_BUTTON1, LWGPIO_DIR_INPUT, LWGPIO_VALUE_NOCHANGE))
    {
        printf("Initializing button GPIO as input failed.\n");
        _task_block();
    }
		lwgpio_set_functionality(&btn1, BSP_BUTTON1_MUX_GPIO);
    lwgpio_set_attribute(&btn1, LWGPIO_ATTR_PULL_UP, LWGPIO_AVAL_ENABLE);
		
		if (!lwgpio_init(&btn2, BSP_BUTTON2, LWGPIO_DIR_INPUT, LWGPIO_VALUE_NOCHANGE))
    {
        printf("Initializing button GPIO as input failed.\n");
        _task_block();
    }
	  lwgpio_set_functionality(&btn2, BSP_BUTTON2_MUX_GPIO);
    lwgpio_set_attribute(&btn2, LWGPIO_ATTR_PULL_UP, LWGPIO_AVAL_ENABLE);
/*----------------------------------------------------------------------------------*/			
		// 
		lwgpio_init(&GPIO_FirstDriveBeginLimit, (GPIO_PORT_B | GPIO_PIN22), LWGPIO_DIR_INPUT, LWGPIO_VALUE_NOCHANGE);
		lwgpio_set_functionality(&GPIO_FirstDriveBeginLimit, LWGPIO_MUX_B22_GPIO);
    lwgpio_set_attribute(&GPIO_FirstDriveBeginLimit, LWGPIO_ATTR_PULL_UP, LWGPIO_AVAL_ENABLE);		//PULL_DOWN?
		
		lwgpio_init(&GPIO_FirstDriveEndLimit, (GPIO_PORT_C | GPIO_PIN12), LWGPIO_DIR_INPUT, LWGPIO_VALUE_NOCHANGE);
	  lwgpio_set_functionality(&GPIO_FirstDriveEndLimit, LWGPIO_MUX_C12_GPIO);
    lwgpio_set_attribute(&GPIO_FirstDriveEndLimit, LWGPIO_ATTR_PULL_UP, LWGPIO_AVAL_ENABLE);			
		
/*----------------------------------------------------------------------------------*/		
}
void LED_Control(uint8_t led, bool state)
{
	if (state == 1) {
		if (led == 1) {
			lwgpio_set_value(&led1, LWGPIO_VALUE_LOW);
		}
		if (led == 2) {
			lwgpio_set_value(&led2, LWGPIO_VALUE_LOW);
		}
		if (led == 3) {
			lwgpio_set_value(&led3, LWGPIO_VALUE_LOW);
		}
		if (led == 4) {
			lwgpio_set_value(&led4, LWGPIO_VALUE_LOW);
		}
	}	else {
			if (led == 1) {
				lwgpio_set_value(&led1, LWGPIO_VALUE_HIGH);
			}
			if (led == 2) {
				lwgpio_set_value(&led2, LWGPIO_VALUE_HIGH);
			}
			if (led == 3) {
				lwgpio_set_value(&led3, LWGPIO_VALUE_HIGH);
			}
			if (led == 4) {
				lwgpio_set_value(&led4, LWGPIO_VALUE_HIGH);
			}
		}
		
}
bool BTN_Read(uint8_t btn)
{
		if (btn == 1){
			if (LWGPIO_VALUE_LOW == lwgpio_get_value(&btn1)) {
			return 1; 
			} else {
				return 0;
			}
		} else {
			if (LWGPIO_VALUE_LOW == lwgpio_get_value(&btn2)) {
			return 1; 
			} else {
				return 0;	
			}
		}
}


void LED_toggle(uint8_t led)
{
	if (led == 1){
		lwgpio_toggle_value(&led1); }
	if (led == 2){
		lwgpio_toggle_value(&led2); }
	if (led == 3){
		lwgpio_toggle_value(&led3); }
	if (led == 4){
		lwgpio_toggle_value(&led4);	}
}

bool LIM_Read(uint8_t lim)
{
	if (lim == 1) {
		if (LWGPIO_VALUE_LOW == lwgpio_get_value(&GPIO_FirstDriveBeginLimit)) {
			return 0; 
		} else {
				return 1;
			}
	}
	if (lim == 2) {
		if (LWGPIO_VALUE_LOW == lwgpio_get_value(&GPIO_FirstDriveEndLimit)) {
			return 0; 
		} else {
				return 1;
			}
	}	
	return 0;
}
/*EOF*/
