
extern LWGPIO_STRUCT led1,led2,led3,led4, btn1, btn2;
extern LWGPIO_STRUCT GPIO_FirstDriveBeginLimit, GPIO_SecondDriveBeginLimit, 
										 GPIO_FirstDriveEndLimit,   GPIO_SecondDriveEndLimit;
extern void LED_init(void);

extern void LED_Control(uint8_t led, bool state);
extern bool BTN_Read(uint8_t btn);
extern bool LIM_Read(uint8_t lim);
extern void LED_toggle(uint8_t led);

extern bool Touch_Read(uint8_t btn);
