#include "Stend_VIRD.h"	
#include <fio.h>
#include "init.h"	
#include "LED/led.h"



#if ! BSPCFG_ENABLE_IO_SUBSYSTEM
#error This application requires BSPCFG_ENABLE_IO_SUBSYSTEM defined non-zero in user_config.h. Please recompile BSP with this option.
#endif


#ifndef BSP_DEFAULT_IO_CHANNEL_DEFINED
#error This application requires BSP_DEFAULT_IO_CHANNEL to be not NULL. Please set corresponding BSPCFG_ENABLE_TTYx to non-zero in user_config.h and recompile BSP with this option.
#endif

#ifndef BSP_DEFAULT_LWADC_MODULE
#error This application requires BSP_DEFAULT_LWADC_MODULE to be not defined in the BSP. Please recompile BSP with this option.
#endif

#ifndef BSP_ADC_POTENTIOMETER
#error This application requires BSP_ADC_POTENTIOMETER to be defined in the BSP. Please recompile BSP with this option.
#endif


#define BSP_ADC_INPUT_1           (ADC1_SOURCE_AD13)

extern const LWADC_INIT_STRUCT BSP_DEFAULT_LWADC_MODULE;

typedef struct adc_demo_struct {
    const char *    name;
    uint32_t         input;
} ADC_DEMO_STRUCT;

/* This structure defines the generic ADCs available on this board. The structure will be populated based on
** what generic ADCs are defined in the BSP. The generic ADCs currently supported are:
**  Potentiometer:                  BSP_ADC_POTENTIOMETER
**  Up to 8 generic ADC inputs:     BSP_ADC_INPUT_0..BSP_ADC_INPUT_7
**  One TWRPI ADC interface:        BSP_ADC_TWRPI_PINx
**  Tower primary elevator          BSP_ADC_TWR_AN0..BSP_ADC_TWR_AN7
**  Tower secondary elevator        BSP_ADC_TWR_AN8..BSP_ADC_TWR_AN13
**  Core voltage:                   BSP_ADC_VDD_CORE
**  Temperature:                    BSP_ADC_TEMPERATURE */

static uint32_t U_Moment=0;

const ADC_DEMO_STRUCT adc_inputs[] = {
        
#ifdef BSP_ADC_POTENTIOMETER 
   {"Potentiometer", BSP_ADC_POTENTIOMETER },
#endif    
   
#ifdef BSP_ADC_INPUT_0 
   {"Generic input #0", BSP_ADC_INPUT_0 },
#endif
   
#ifdef BSP_ADC_INPUT_1 
   {"Generic input #1", BSP_ADC_INPUT_1 },
#endif
   
#ifdef BSP_ADC_INPUT_2 
   {"Generic input #2", BSP_ADC_INPUT_2 },
#endif
   
#ifdef BSP_ADC_INPUT_3 
   {"Generic input #3", BSP_ADC_INPUT_3 },
#endif
   
#ifdef BSP_ADC_INPUT_4 
   {"Generic input #4", BSP_ADC_INPUT_4 },
#endif
   
#ifdef BSP_ADC_INPUT_5 
   {"Generic input #5", BSP_ADC_INPUT_5 },
#endif
   
#ifdef BSP_ADC_INPUT_6 
   {"Generic input #6", BSP_ADC_INPUT_6 },
#endif
   
#ifdef BSP_ADC_INPUT_7 
   {"Generic input #7", BSP_ADC_INPUT_7 },
#endif
   
#ifdef BSP_ADC_TWRPI_PIN8 
   {"TWRPI Pin#8", BSP_ADC_TWRPI_PIN8 },
#endif                
#ifdef BSP_ADC_TWRPI_PIN9 
   {"TWRPI Pin#9", BSP_ADC_TWRPI_PIN9 },
#endif                
#ifdef BSP_ADC_TWRPI_PIN12 
   {"TWRPI Pin#12", BSP_ADC_TWRPI_PIN12 },
#endif                
#ifdef BSP_ADC_TWRPI_PIN17 
   {"TWRPI Pin#17", BSP_ADC_TWRPI_PIN17 },
#endif                
#ifdef BSP_ADC_TWRPI_PIN18 
   {"TWRPI Pin#18", BSP_ADC_TWRPI_PIN18 },
#endif                

#ifdef BSP_ADC_TWR_AN0 
   {"Primary Elevator AN0", BSP_ADC_TWR_AN0 },
#endif                
#ifdef BSP_ADC_TWR_AN1 
   {"Primary Elevator AN1", BSP_ADC_TWR_AN1 },
#endif                
#ifdef BSP_ADC_TWR_AN2 
   {"Primary Elevator AN2", BSP_ADC_TWR_AN2 },
#endif                
#ifdef BSP_ADC_TWR_AN3 
   {"Primary Elevator AN3", BSP_ADC_TWR_AN3 },
#endif                
#ifdef BSP_ADC_TWR_AN4 
   {"Primary Elevator AN4", BSP_ADC_TWR_AN4 },
#endif                
#ifdef BSP_ADC_TWR_AN5 
   {"Primary Elevator AN5", BSP_ADC_TWR_AN5 },
#endif                
#ifdef BSP_ADC_TWR_AN6 
   {"Primary Elevator AN6", BSP_ADC_TWR_AN6 },
#endif                
#ifdef BSP_ADC_TWR_AN7 
   {"Primary Elevator AN7", BSP_ADC_TWR_AN7 },
#endif                
      
#ifdef BSP_ADC_TWR_AN8 
   {"Secondary Elevator AN8", BSP_ADC_TWR_AN8 },
#endif                
#ifdef BSP_ADC_TWR_AN9 
   {"Secondary Elevator AN9", BSP_ADC_TWR_AN9 },
#endif                
#ifdef BSP_ADC_TWR_AN10 
   {"Secondary Elevator AN10", BSP_ADC_TWR_AN10 },
#endif                

#ifdef BSP_ADC_TWR_AN11 
   {"Secondary Elevator AN11", BSP_ADC_TWR_AN11 },
#endif                
#ifdef BSP_ADC_TWR_AN12 
   {"Secondary Elevator AN12", BSP_ADC_TWR_AN12 },
#endif                
#ifdef BSP_ADC_TWR_AN13 
   {"Secondary Elevator AN13", BSP_ADC_TWR_AN13 },
#endif                

#ifdef BSP_ADC_VDD_CORE 
   {"Core VDD", BSP_ADC_VDD_CORE },
#endif                
#ifdef BSP_ADC_TEMPERATURE 
   {"Temperature", BSP_ADC_TEMPERATURE },
#endif                

};




static void monitor_all_inputs(void)
{
    LWADC_STRUCT_PTR    lwadc_inputs;
    LWADC_VALUE_PTR     last;
    LWADC_VALUE         i,scaled, raw;		
	

    lwadc_inputs = (LWADC_STRUCT_PTR) _mem_alloc_zero(ELEMENTS_OF(adc_inputs)*sizeof(LWADC_STRUCT));
    
    if (lwadc_inputs == NULL)  {
        printf("Error, Insufficient memory to run full test\n.");
        _task_block();
    }
    
    for (i=0;i<ELEMENTS_OF(adc_inputs);i++) {
        /* Set last value to a value out of range of the ADC. */
        if ( !_lwadc_init_input(&lwadc_inputs[i],adc_inputs[i].input) ) {
            /* Failed to initialize this input. We will end up failing the reads below as well. */
            printf("Failed to initialize ADC input %s\n",adc_inputs[i].name);
        }
    }
    
    printf("Monitoring ADC Inputs\n");
    while (1) {
        for (i=0;i<ELEMENTS_OF(adc_inputs);i++) {
            /* This waits until a new conversion is read on the channel */
            if (_lwadc_wait_next(&lwadc_inputs[i])) {
                if (_lwadc_read(&lwadc_inputs[i], &scaled) &&
                    _lwadc_read_raw(&lwadc_inputs[i], &raw)) {
                    /* Obtained data, is the change significant enough to display? */
											Set_UMoment(scaled);
                }
            }
        }
							_time_delay(100);
    }
}

void adc_task(uint32_t initial_data)
{

    _lwadc_init(&BSP_DEFAULT_LWADC_MODULE);

    monitor_all_inputs();
}

void Set_UMoment(uint32_t data)
{
	U_Moment =data;
}

uint32_t Get_UMoment()
{
	return U_Moment;
}
